#!/usr/bin/env bash
# shellcheck shell=bash disable=SC2086
# set -x
exec &> >(tee -a build.log)

__buildDocker() {
    docker "${__buildArgs[@]}" --tag "${__buildImage}":"${__buildTags}" --file "${__buildFile}" .
}

__buildImage="${CI_REGISTRY_IMAGE}"
__buildTags="latest"
__buildArgs=( "buildx" "build" "--platform" "${ARCH}" "--no-cache" "--pull" "--push" "--compress" "--rm" )
__buildFile='Dockerfile'

__buildDocker
